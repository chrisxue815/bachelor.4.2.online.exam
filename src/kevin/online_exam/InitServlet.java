package kevin.online_exam;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

/**
 * Servlet implementation class InitServlet
 */
@WebServlet(name="InitServlet", urlPatterns="/InitServlet", loadOnStartup=0)
public class InitServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public InitServlet() {
    super();
  }

  /**
   * @see Servlet#init(ServletConfig)
   */
  public void init(ServletConfig config) throws ServletException {
    Connection connection = null;
    try {
      Class.forName("com.mysql.jdbc.Driver");

      connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/online.exam", "kevin", "1234");
      Statement statement = connection.createStatement();

      statement.executeUpdate("CREATE TABLE IF NOT EXISTS Users(name VARCHAR(50) PRIMARY KEY, score INT)");
      statement.executeUpdate("CREATE TABLE IF NOT EXISTS Topics(id INT PRIMARY KEY, question VARCHAR(50), answer BOOL)");

      statement.executeUpdate("INSERT IGNORE INTO Topics(id, question, answer) VALUES(1, 'q1', 1)");
      statement.executeUpdate("INSERT IGNORE INTO Topics(id, question, answer) VALUES(2, 'q2', 1)");
      statement.executeUpdate("INSERT IGNORE INTO Topics(id, question, answer) VALUES(3, 'q3', 1)");
      statement.executeUpdate("INSERT IGNORE INTO Topics(id, question, answer) VALUES(4, 'q4', 1)");
      statement.executeUpdate("INSERT IGNORE INTO Topics(id, question, answer) VALUES(5, 'q5', 1)");
      statement.executeUpdate("INSERT IGNORE INTO Topics(id, question, answer) VALUES(6, 'q6', 1)");
      statement.executeUpdate("INSERT IGNORE INTO Topics(id, question, answer) VALUES(7, 'q7', 1)");
      statement.executeUpdate("INSERT IGNORE INTO Topics(id, question, answer) VALUES(8, 'q8', 1)");
      statement.executeUpdate("INSERT IGNORE INTO Topics(id, question, answer) VALUES(9, 'q9', 1)");
      statement.executeUpdate("INSERT IGNORE INTO Topics(id, question, answer) VALUES(10, 'q10', 1)");

      statement.executeBatch();
    }
    catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    finally {
      try {
        if (connection != null)
          connection.close();
      }
      catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

}
