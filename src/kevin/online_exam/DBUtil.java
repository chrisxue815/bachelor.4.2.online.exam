package kevin.online_exam;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class DBUtil {

  public static String getQuestion(HttpServletRequest request) {
    String question = null;
    String username = request.getRemoteUser();
    String answerStr = request.getParameter("answer");
    
    HttpSession session = request.getSession();
    Object topicIdObj = session.getAttribute("topicId");
    Object scoreObj = session.getAttribute("score");
    int topicId = (topicIdObj == null) ? 1 : (Integer)topicIdObj;
    int score = (scoreObj == null) ? 0 : (Integer)scoreObj;
    
    try {
      Connection connection = DriverManager.getConnection(
          "jdbc:mysql://127.0.0.1:3306/online.exam", "kevin", "1234");
      PreparedStatement statement = null;

      if (answerStr != null) {
        boolean answer = answerStr.equals("true") ? true : false;
        
        statement = connection.prepareStatement("SELECT answer FROM Topics where id=?");
        statement.setInt(1, topicId);
        ResultSet rs = statement.executeQuery();
        
        if (rs.next()) {
          boolean correctAnswer = rs.getBoolean("answer");
          rs.close();
          
          ++topicId;
          session.setAttribute("topicId", topicId);
          
          if (correctAnswer == answer) {
            ++score;
            session.setAttribute("score", score);
          }
        }
      }
      
      statement = connection.prepareStatement("SELECT question FROM Topics WHERE id=?");
      statement.setInt(1, topicId);
      ResultSet rs = statement.executeQuery();

      if (rs.next()) {
        question = rs.getString("question");
        rs.close();
      }
      else {
        rs.close();
        
        statement = connection.prepareStatement("INSERT INTO Users(name,score) VALUES(?,?) ON DUPLICATE KEY UPDATE score=?");
        statement.setString(1, username);
        statement.setInt(2, score);
        statement.setInt(3, score);
        statement.executeUpdate();
        
        session.setAttribute("topicId", 1);
        session.setAttribute("score", 0);
      }
      
      return question;
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    
    return null;
  }
  
  public static int getScore(String username) {
    try {
      Connection connection = DriverManager.getConnection(
          "jdbc:mysql://127.0.0.1:3306/online.exam", "kevin", "1234");

      PreparedStatement statement = connection.prepareStatement("SELECT score FROM Users WHERE name=?");
      statement.setString(1, username);
      ResultSet rs = statement.executeQuery();
      
      int score = -1;
      if (rs.next()) {
        score = rs.getInt("score");
      }

      rs.close();
      connection.close();

      return score;
    }
    catch (SQLException e) {
      e.printStackTrace();
    }

    return -1;
  }
}
