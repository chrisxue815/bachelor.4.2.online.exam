<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8" import="kevin.online_exam.DBUtil"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Online exam</title>
</head>
<body>
  <%
    int score = DBUtil.getScore(request.getRemoteUser());
    if (score == -1) {
  %>
  <div>
    Welcome
    <%=request.getRemoteUser()%>. Let's start the game.
  </div>
  <form action="topic.jsp" method="get">
    <input type="submit" value="Begin" />
  </form>
  <%
    }
    else {
  %>
  <div>
    You have already played the game - your previous score was
    <%=score%>
    / 10
  </div>
  <form action="topic.jsp" method="get">
    <input type="submit" value="Retry" />
  </form>
  <%
    }
  %>
</body>
</html>