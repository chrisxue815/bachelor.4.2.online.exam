<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8" import="kevin.online_exam.DBUtil"%>
<%
String question = DBUtil.getQuestion(request);
if (question == null) {
  response.sendRedirect("index.jsp");
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Topic</title>
</head>
<body>
  <div><%=question%></div>
  <form action="topic.jsp" method="post">
    <div>
      <input type="radio" name="answer" value="true" checked="checked" />True
    </div>
    <div>
      <input type="radio" name="answer" value="false" />False
    </div>
    <div>
      <input type="submit" value="Submit" /> <input type="reset"
        value="Reset" />
    </div>
  </form>
</body>
</html>
